training_data_path = "/home/vishwam/mountpoint/madhavi/East_Text_Detection/training/madhavi/PASSPORT_POI/" #encrypted
checkpoint_path = '/home/vishwam/mountpoint/madhavi/East_Text_Detection/training/madhavi/ckpts/'
pretrained_model_path = '/home/vishwam/mountpoint/madhavi/East_Text_Detection/training/madhavi/best_model/model.ckpt-56561'
temp_file_path = "/home/vishwam/mountpoint/madhavi/East_Text_Detection/training/madhavi/temp1.jpeg"
total_steps = 100000
save_checkpoint_steps = 500
max_checkpoints_to_keep = 15
batch_size_per_gpu = 16
learning_rate = 0.0001
num_readers = 24
geometry = "RBOX"
gpus = 4
restore = False
decryption_file = '/key/dkyc/decrypt_ubuntu2004.so'
# training_data_path = "/home/vishwam/mountpoint/chandra/EAST-master-py36/DATA/decrypted_samples/" # original

