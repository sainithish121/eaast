import ctypes, os
import base64, glob
import tensorflow as tf 
enc_imgs = "/home/vishwam/mountpoint/chandra/EAST-master-py36/DATA/sample_data_encrypted/images/"
output_path = "/home/vishwam/mountpoint/chandra/EAST-master-py36/DATA/decrypted_samples/"
os.makedirs(output_path,exist_ok=True)
key = ctypes.cdll.LoadLibrary('/key/dkyc/decrypt_ubuntu2004.so')
# get the decryptImage function & assign to python function
decrypt_img = key.decryptImage
# set decrypt function input & return types
decrypt_img.argtypes = [ctypes.c_char_p]
decrypt_img.restype = ctypes.c_void_p

def decrypt_image(img_path,decrypt_img):
    # try:
        rawp = decrypt_img(img_path.encode('utf-8'))
        raw_bytes = ctypes.string_at(rawp)
        raw_string = raw_bytes.decode('utf-8')
        # convert base64 string to obtain decrypted image
        imgdata = base64.b64decode(raw_string)
        # print("Image base64 string in Python:",len(raw_string))
        #saving file for reference
        filename = os.path.basename(img_path)
        filename = os.path.join(output_path,filename)
        with open(filename,'wb') as f:
            f.write(imgdata)
            
        # image = tf.keras.preprocessing.image.load_img(filename)
        
        # return image
    # except :
    #     corrupt_img = [img_path]
    #     unidentified_imgs.append(corrupt_img)
    #     return None



for img in os.listdir(enc_imgs):
    if img.endswith((".jpg",".png")):
        decrypt_image(os.path.join(enc_imgs,img),decrypt_img)


