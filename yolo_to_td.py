from PIL import Image
import csv
import os,sys
import time
import glob

def Yolo_ltrb(input_images_path,input_labels_path,output_labels_path):
    all_images = os.listdir(input_images_path)
    all_images.sort()

    for each_image in all_images:
        original_img_file = Image.open(os.path.join(input_images_path, each_image))
        original_im_width = original_img_file.width
        original_im_height = original_img_file.height

        all_things = []

        reader = csv.reader(open(os.path.join(input_labels_path,each_image[0:-4] +'.txt')), delimiter=" ")
        i = 0
        for row in reader:
            i += 1
            temp = [int(row[0]), float(row[1]) * original_im_width * 2, float(row[2]) * original_im_height * 2, float(row[3]) * original_im_width, float(row[4]) * original_im_height]
            p3 = (temp[1] + temp[3]) / 2
            p4 = (temp[2] + temp[4]) / 2
            p1 = temp[1] - p3
            p2 = temp[2] - p4
            #cropped_image = original_img_file.crop((int(p1), int(p2), int(p3), int(p4)))
            # cropped_image.save(os.path.join(output_labels_path,'temp.jpg'))
            all_things.append([round(p1), round(p2),round(p3),round(p2), round(p3), round(p4),round(p1),round(p4),0])
            with open(os.path.join(output_labels_path, each_image[0:-4] + '.txt'), 'w') as f:
                writer = csv.writer(f, delimiter = ',')
                for p in all_things:
                    writer.writerow(p)
                    

Yolo_ltrb("/home/vishwam/mountpoint/chandra/EAST-master-py36/images/","/home/vishwam/mountpoint/chandra/EAST-master-py36/DATA/sample_data_encrypted/labels/","/home/vishwam/mountpoint/chandra/EAST-master-py36/DATA/sample_data_encrypted/td_labels/")  
