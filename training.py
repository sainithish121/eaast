import logging, datetime, os, ntpath, shutil, glob, csv, io, ntpath, time, pandas as pd, math, gcloud, yaml, keras, random, zipfile, numpy as np, ctypes, base64, matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.preprocessing.image import load_img
from gcloud import storage
from  efficientnet.tfkeras import EfficientNetB0, EfficientNetB1, EfficientNetB2, EfficientNetB3, EfficientNetB4, EfficientNetB5, EfficientNetB6, EfficientNetB7
from PIL import ImageFile
from tensorflow.keras.callbacks import Callback
from oauth2client.service_account import ServiceAccountCredentials
from architectures import ArchitectureSearch

ImageFile.LOAD_TRUNCATED_IMAGES = True
logging.getLogger('tensorflow').setLevel(logging.ERROR)
print("Num GPUs Available >>>>>>: ", len(tf.config.list_physical_devices('GPU')))

train_time = str(datetime.datetime.now())
train_time = train_time.replace(".", "_")
train_time = train_time.replace(":", "_")
train_time = train_time.replace(" ", "_")
GPU_COMMAND = "nvidia-smi"

with open('hyperparameter.yaml') as file:
    hyperparameters = yaml.load(file, Loader = yaml.FullLoader)
gpus = hyperparameters['gpus']

os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = str(gpus)

credentials_dict = {
'type': 'service_account',
'client_id': "112001579524151631326",
'client_email': "mlops-models-rw@jiovishwamprod.iam.gserviceaccount.com",
'private_key_id': "d3373cca58f6ac26195b832b693a8af04012e95e",
'private_key': "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDgoBGk2EeWHopT\nLUH+/EAMLePmQ4FdYNAQb3JABd7EiUk6qZpV92/3uuk7+eMWOi9QLrTaJMHynK9O\n28HoCcmSHA3fke/Q+QMF8DJRxCHQpOYLiR44AWIR0KHU5XaWEOvyLMm9AC8GhbbX\nd8c8/+MJuvTDE4Vo822xLYbj2a+cj5p4QekN3lIdneoX1qZrLWdndJ/IMO++Tq3V\naOhqqF2ZIMS02tbhFleSG3/xUh7CMjuSr2GCPbGzrfS250NYqajutZMCjt9y00fh\nx/2mj+nElFVd4BYubGQRh8uxyzDOUWoqAxV9I5yhNvuBBfNx9tznW/F9JRC30r4/\noCSnQzptAgMBAAECggEABG+c0ZbU2RXa0PgylrTIFgV8+/Hm1t9/1GdDCBkllFeK\nuu4ZBmwXybpm+0B819ZH02tMtoclzmMCQkosOucglDJaIsloxUV5pg8fi0ZWjZ6I\nDX/EeUIru+LVI1VxwVZttm9kf4dGpngTC4/Jvz5lby6CQWz8BD3Uc3yM7qiAo5SX\n8mMimeWQyCE8/7m0cygH2hZ0fFfNwcw5F3zbTPSMEzmG9CXVtQ5k5o1wT4UQtpXs\nldTzHG0lJo9Mgf7G/QLEgRR1VvNVAoXsdY8OfWY8GgykQM7hwYePUgL/FUB2G3VB\n9B0+kc/IHbvovyFyaP3X/eVb2OrEAQ2vzJ6NssRkcQKBgQD8EcsDBV6iL+TjE1VY\n9N+ts6oRjCASkZ8B4EZaqt9VTzg3MmTVyii1jTh/1hvUY+7kdULmAOY8Sum4CCRO\n+4bE2gudu3dPW2rhdpL3TKCseO8zFeIf4zeinkSACwlawAGVXzgSL41oaAtk7aOe\nnQyJ/9JWrFL3mOM5QSopRu+UkQKBgQDkILl1uiNHv9RlnySM1Zi9F0W4Qdf6Lmu/\n/gvMovyA15xEMNyXVrKymGB7GGsBy8Q4pyI4eexJTyllt4597LyHDASEGxj0jU78\njQcG9BLXxNjZAcvoEeGP1pf71xwopwngr3UugsY71d7lV0PDdl39TzaUlHhQPkxd\nha9UeGQGHQKBgQCbjHdUo+mj+B4sqm+Od33YePhH38qi6s3OoJGJvcwXns038pca\n/qdkcYMeEAgYZpqyzFG+EWwCZNAZSpztSHX/3/YvHdIgXG92Nm+2v8ie5fnl8mkZ\n+GMKrNPkl+rr33SasqmtPz7p4Nq3K5Y+ePeV6RHpfmmsdOIzIK1dz3JkoQKBgBZ/\nP6rje101Br4qzpDw84oaRG7iV2k9sLSTWhiJLjuKh8eVxftKun6wVjttcggQTz6D\nBPXq2Cl2nA+C5ZN3iDQNNFjOz0w2r0ZqpfnCkiUjLXthGPm+4a6sq/pTAVzk9C1r\nQDQhA2mdMskNLQqWnrg/WpOWfLL1IdTLPTooUTzlAoGAKCLrgWDf5ZDZnTKD8DCB\n5csKL5Ere+kBQjFEH99jVmoWs29XSb8ogewymEl3CMSlRQdgfc4Yzl6PzWGAiq8D\nFp38kLrTcSCc3q5N+S6NpR+W24JU7yFLz32GG72okpOYpGIbUyEmW4KLT1e5iwAt\nrUVKzRRDh3mxXi19uVbiUd4=\n-----END PRIVATE KEY-----\n",
}

credentials = ServiceAccountCredentials.from_json_keyfile_dict(credentials_dict)
client = storage.Client(credentials = credentials, project = 'jiovishwamprod')
bucket = client.get_bucket('mlops-pipeline-models')

class TrainAndTest(Callback):

    def __init__(self):

        super(TrainAndTest, self).__init__()
        
        self.input_shape = 224
        self.gpus = gpus
        self.gpu_command = GPU_COMMAND
        self.train_time = train_time
        self.namespace = hyperparameters["namespace"]

        self.project_name = hyperparameters["project_name"]
        self.data_set_name = hyperparameters["data_set_name"]
        
        self.train_data_dir = '/data/' + str(self.project_name) + '/'+ str(self.data_set_name) + '/training/train'
        self.val_data_dir = '/data/' + str(self.project_name) + '/'+ str(self.data_set_name) + '/training/validation'
        self.evaluation_data_dir = '/data/' + str(self.project_name) +'/'+  str(self.data_set_name) + '/evaluation/*/*'
        self.path_to_save_final_model = 'imageclassification/classifier-models/' + str(self.project_name) + '/' + str(self.namespace) + '/'
        self.folder_uploading_path = str(self.data_set_name)+'_'+str(self.train_time)
        self.logdir = self.path_to_save_final_model

        self.decrypt = ctypes.cdll.LoadLibrary('/key/dkyc/decrypt_ubuntu1804.so')
        # self.decrypt = ctypes.cdll.LoadLibrary('/home/vishwam/mountpoint/kokkonda/Archive/decrypt_ubuntu1804.so')

        self.decrypt = self.decrypt.decryptImage
        self.decrypt.argtypes = [ctypes.c_char_p]
        self.decrypt.restype = ctypes.c_void_p
        
        self.num_classes = len(os.listdir('/data/' + str(self.project_name) + '/'+ str(self.data_set_name) + '/' + 'evaluation/'))
        self.epochs = hyperparameters["epochs"]
        self.batch_size = hyperparameters["batch_size"]
        self.learning_rate = hyperparameters["learning_rate"]
        self.patience = 2
        self.unidentified_imgs = []
        self.modified_unidentified_img_lst = []
        
    def gpu_strategy(self):
        # strategy = tf.distribute.MirroredStrategy(["GPU:0"])
        if str(self.gpus) == '0':
            strategy = tf.distribute.MirroredStrategy(["GPU:0"])
        elif str(self.gpus) == '0,1':
            strategy = tf.distribute.MirroredStrategy(["GPU:0", "GPU:1"])
        elif str(self.gpus) == '0,1,2,3':
            strategy = tf.distribute.MirroredStrategy(["GPU:0", "GPU:1", "GPU:2", "GPU:3"])
        return strategy
    
    def gpu_workers(self):

        if str(self.gpus) == "0" or str(self.gpus) == "1":
            workers = 6
        elif str(self.gpus) == "0,1":
            workers = 12
        elif str(self.gpus) == "0,1,2,3":
            workers = 24
        
        return workers
    
    def decrypt_image(self, decrypt, image_dir_path):
        try:
            img_name = image_dir_path.split("/")[-1]
            rawp = decrypt(image_dir_path.encode('utf-8'))  
            raw_bytes = ctypes.string_at(rawp)
            raw_string = raw_bytes.decode('utf-8')
            # convert base64 string to obtain decrypted imag
            imgdata = base64.b64decode(raw_string)
            filename = "temp.jpeg"
            with open(filename,'wb') as f:
                f.write(imgdata)
            image = tf.keras.preprocessing.image.load_img(filename, target_size = (224,224), interpolation = "lanczos")       
            return image
        except :
            corrupt_img = [img_name, image_dir_path.split('/')[-2], image_dir_path.split('/')[-3]]
            self.unidentified_imgs.append(corrupt_img)
        
            return None

    def fetch_decrypted_images(self, img_dir):

        print("dir path >>>", img_dir)
        self.num_examples = len(glob.glob(img_dir + "/*/"))
        self.classes = os.listdir(img_dir + "/")
        self.len_classes = len(self.classes)
        print("classess >>>>>", self.classes, "Number of classes >>>>>", self.len_classes)            
        sorted(self.classes)

        img_list = []
        img_class_list = []

        for img_path in glob.glob(img_dir + "/*/*" ):
            dec_img = self.decrypt_image(self.decrypt, img_path) 
            print(dec_img)               
            img_class = 0 if self.classes[0] == img_path.split("/")[-2] else 1
            if dec_img == None:
                print("I'm here")
                dec_img = self.decrypt_image(self.decrypt, img_path)        
            else:     
                img_arr = tf.keras.preprocessing.image.img_to_array(dec_img)
                img_arr = img_arr / 255.
                # tf.image.resize(img_arr, [224, 224])

                img_list.append(img_arr)
                img_class_list.append(img_class)
        
        imgs_arr = np.array(img_list, dtype = np.float16)
        img_classes_arr = np.expand_dims(np.array(img_class_list), axis=1)
        print("Shape of an images and classes >>>>>>>",imgs_arr.shape, img_classes_arr.shape)

        return imgs_arr, img_classes_arr

    def configure_for_performance(self, dataset, batch_size):
        dataset = dataset.batch(batch_size)
        dataset = dataset.prefetch(buffer_size = tf.data.experimental.AUTOTUNE)
        return dataset

    def dataset(self, data_arr, batch_size, class_arr):

        image_data_train = tf.data.Dataset.from_tensor_slices(data_arr)
        image_data_classes = tf.data.Dataset.from_tensor_slices(class_arr)

        image_class_zip = tf.data.Dataset.zip((image_data_train,image_data_classes))
        # image_train_dataset = image_class_zip.prefetch(buffer_size = tf.data.experimental.AUTOTUNE)
        
        image_train_dataset = self.configure_for_performance(image_class_zip, batch_size)       
        # image_label_dataset = self.configure_for_performance(class_arr, batch_size)
        return image_train_dataset
    
    def loss_func(self):
        if len(self.classes) <= 2:
            loss = "binary_crossentropy"
        elif len(self.classes) > 2:
            loss = "categorical_crossentropy"
        return loss
    
    def train(self):

        with self.gpu_strategy().scope():

            model_arch_class = ArchitectureSearch()
            model = model_arch_class.load_model()

        train_arr, train_labels = self.fetch_decrypted_images(self.train_data_dir)
        val_arr, val_labels = self.fetch_decrypted_images(self.val_data_dir)

        train_ds = self.dataset(train_arr, self.batch_size, train_labels)
        val_ds = self.dataset(val_arr, self.batch_size, val_labels)

        optimiser = tf.keras.optimizers.Adam(learning_rate = self.learning_rate)        

        model.compile(loss = self.loss_func(), optimizer = optimiser, metrics = ['accuracy'])

        checkpoints_filepath = os.path.join(str(self.path_to_save_final_model),
            str(self.folder_uploading_path)+'/'+
            'model_epoch_{epoch:02d}_'+f'lr_{self.learning_rate}_''val_acc_{val_accuracy:.03f}_'f'tf_{tf.__version__}''.h5')

        checkpoint = tf.keras.callbacks.ModelCheckpoint(
            checkpoints_filepath,
            monitor = 'val_accuracy',
            verbose = 2,
            save_best_only = True,
            mode = 'max')

        early_stopping = tf.keras.callbacks.EarlyStopping(
            monitor = 'val_accuracy',
            min_delta = 0,
            patience = self.patience,
            verbose = 2,
            mode = 'max',
            baseline = None,
            restore_best_weights = True)
        
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir = os.path.join(str(self.path_to_save_final_model),str(self.folder_uploading_path)))

        callbacks_list = [checkpoint, early_stopping, tensorboard_callback]

        # print(model.summary())

        model.fit(
            train_ds,
            # batch_size = self.batch_size,
            epochs = self.epochs,
            verbose = 1,
            validation_data = val_ds,
            validation_batch_size = 1, 
            # steps_per_epoch = math.ceil(self.num_examples/self.batch_size),
            callbacks = callbacks_list,
            use_multiprocessing = True,
            workers = self.gpu_workers(),
            shuffle = True
            #class_weight ={0:1, 1:1.13}
            )
        model.save(str(self.path_to_save_final_model) + str(self.folder_uploading_path) + '/final_epoch_model_'+f'tf_{tf.__version__}''.h5')
        
    def test(self):

        def path_leaf(path):
            head, tail = ntpath.split(path)
            return tail or ntpath.basename(head)

        def preprocessing_img(img_path):
            dec_img = self.decrypt_image(self.decrypt, img_path)
            # img = tf.keras.preprocessing.image.load_img(img_path, target_size = (224, 224), interpolation = "lanczos")
            img_array = tf.keras.preprocessing.image.img_to_array(dec_img)
            # tf.image.resize(img_array, [224, 224])
            img_array = np.expand_dims(img_array, axis = 0)
            img_array /= 255.
            return img_array
        
        models = glob.glob(str(self.path_to_save_final_model) + str(self.folder_uploading_path) + "/*.h5")

        for model in models:

            print(str(os.path.split(model)[-1].split('.h5')[0]))
            if str(os.path.split(model)[-1].split('.h5')[0]) != 'h5':
                name = os.path.split(model)[1]
            
            model = tf.keras.models.load_model(model)       
            imgs_dir = glob.glob(self.evaluation_data_dir)

            with open(str(self.path_to_save_final_model) + str(self.folder_uploading_path)+ '/' + str(name)+'.csv', 'w', newline = '') as csv_file:
                # print(self.num_classes)
                if self.num_classes <= 2:
                    row = ['image_name', 'pred_score' , 'GT']
                elif self.num_classes == 3:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'GT']
                elif self.num_classes == 4:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'class_4', 'GT']
                elif self.num_classes == 5:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'class_4', 'class_5', 'GT']
                elif self.num_classes == 6:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'class_4', 'class_5', 'class_6', 'GT']
                elif self.num_classes == 7:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'class_4', 'class_5', 'class_6', 'class_7', 'GT']
                elif self.num_classes == 8:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'class_4', 'class_5', 'class_6', 'class_7', 'class_8', 'GT']
                elif self.num_classes == 9:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'class_4', 'class_5', 'class_6', 'class_7', 'class_8', 'class_9', 'GT']
                elif self.num_classes == 10:
                    row = ['image_name', 'class_1', 'class_2', 'class_3', 'class_4', 'class_5', 'class_6', 'class_7', 'class_8', 'class_9', 'class_10', 'GT']

                csv_writer  = csv.writer(csv_file)
                csv_writer.writerow(row)
                count = 0

                for image_path in imgs_dir[:]:
                    ground_truth = image_path.split("/")[-2]
                    filename = path_leaf(image_path)
                    preproc_image = preprocessing_img(image_path)
                    model_pred = model.predict(preproc_image)
                    count += 1
                    if count % 100 == 0:
                        print("completed >>>> ", count)
                    if self.num_classes <= 2:
                        row = [filename, model_pred[0][0], ground_truth]
                    elif self.num_classes == 3:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], ground_truth]
                    elif self.num_classes == 4:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], model_pred[0][3], ground_truth]
                    elif self.num_classes == 5:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], model_pred[0][3], model_pred[0][4], ground_truth]
                    elif self.num_classes == 6:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], model_pred[0][3], model_pred[0][4], model_pred[0][5], ground_truth]
                    elif self.num_classes == 7:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], model_pred[0][3], model_pred[0][4], model_pred[0][5], model_pred[0][6], ground_truth]
                    elif self.num_classes == 8:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], model_pred[0][3], model_pred[0][4], model_pred[0][5], model_pred[0][6], model_pred[0][7], ground_truth]
                    elif self.num_classes == 9:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], model_pred[0][3], model_pred[0][4], model_pred[0][5], model_pred[0][6], model_pred[0][7], model_pred[0][8], ground_truth]
                    elif self.num_classes == 10:
                        row = [filename, model_pred[0][0], model_pred[0][1], model_pred[0][2], model_pred[0][3], model_pred[0][4], model_pred[0][5], model_pred[0][6], model_pred[0][7], model_pred[0][8], model_pred[0][9], ground_truth]
                    csv_writer.writerow(row)
                print(name, 'tested')

        # Unidentified Non-Duplicate Images to csv
        if not self.unidentified_imgs:
            print(f">>>>>>>>>> Hey {self.namespace}, Undentified images are Null <<<<<<<<<<<<<<")
        else:            
            df = pd.DataFrame(self.unidentified_imgs, columns = ['img_name', 'class', 'directory'])
            print("Duplicates>>>>>",df)
            unidentified_non_dup_imgs = df.drop_duplicates(subset = ['img_name'])    
            print("Non-duplicates>>>>>",unidentified_non_dup_imgs)
            unidentified_non_dup_imgs.to_csv(str(self.path_to_save_final_model) + str(self.folder_uploading_path)+ '/' +'images_undentified.csv', index = False) 

    def model_register(self):
        ''' model register function '''
        shutil.make_archive((str(self.path_to_save_final_model) + str(self.folder_uploading_path)), 'zip', self.path_to_save_final_model, str(self.folder_uploading_path))
        for lst in os.listdir(str(self.path_to_save_final_model)):
            if lst == str(self.folder_uploading_path) + ".zip":
                upload_file_path_main = str(self.path_to_save_final_model) + str(self.folder_uploading_path) + ".zip"
                # remote_path = os.path.join(self.project_name, str(self.folder_uploading_path) + ".zip")
                print("upload_file_path_main :", upload_file_path_main)
                blob = bucket.blob(upload_file_path_main)
                blob.upload_from_filename(upload_file_path_main)

        # shutil.make_archive((str(self.path_to_save_final_model) + str(self.folder_uploading_path)), 'zip', self.path_to_save_final_model, str(self.folder_uploading_path))
        # upload_local_directory_to_gcs(os.path.join(self.path_to_save_final_model,self.folder_uploading_path) , bucket, self.project_name)